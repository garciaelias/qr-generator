package com.elias.qrgenerator.controller;

import java.io.IOException;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elias.qrgenerator.model.Customer;
import com.elias.qrgenerator.service.QrService;
import com.google.zxing.WriterException;

@RestController
@RequestMapping("/v1")
public class QrController {

	@GetMapping(value = "/qr/{code}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getQrCode(@PathVariable("code") String code) throws Exception {
		return QrService.getQRCodeImage(code, 250, 250);
	}

	@PostMapping(value = "/qr", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] postController(@RequestBody Customer customer) throws WriterException, IOException {

		return QrService.getQRCodeImage(customer.toString(), 250, 250);

	}
}
