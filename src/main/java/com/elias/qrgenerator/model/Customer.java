package com.elias.qrgenerator.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Customer implements Serializable {

	private String business_name;
	private String cuit;
	private String merchant_number;

	@Override
	public String toString() {
		return "Customer [businessName=" + business_name + ", cuit=" + cuit + ", merchant_number=" + merchant_number
				+ "]";
	}

	public String getBusiness_name() {
		return business_name;
	}

	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getMerchant_number() {
		return merchant_number;
	}

	public void setMerchant_number(String merchant_number) {
		this.merchant_number = merchant_number;
	}

	public Customer(String business_name, String cuit, String merchant_number) {
		this.business_name = business_name;
		this.cuit = cuit;
		this.merchant_number = merchant_number;
	}

	public Customer() {
		this.business_name = "";
		this.cuit = "";
		this.merchant_number = "";
	}

}
